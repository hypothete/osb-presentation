# OSB Presentation #

Review of my favorite presentations from the 2017 Open Source Bridge Conference

### Setup ###

* Clone the repo
* Navigate into the repo
* Start your favorite web server
* Navigate to http://localhost:[port]

### Links in the presentation ###

* [Asterank](http://www.asterank.com/)
* [toot.cat (Mastodon instance)](https://toot.cat/about)
* [Santi Adavani's Singular Value Decomposition slides](https://drive.google.com/file/d/0B8QEfTelhxzcVXlGYmhacF9ld3M/view)
* [ddbeck's readme checklist](https://github.com/ddbeck/readme-checklist/blob/master/checklist.md)
* [keepachangelog.com](http://keepachangelog.com)
* [A-Frame](https://aframe.io/)
* [VR Pet Vue.js demo](https://github.com/hypothete/vr-pet)
* [A-Frame AR plugin](https://github.com/jeromeetienne/AR.js/tree/master/aframe)
* [K-Frame](https://github.com/ngokevin/kframe)
* [A-Frame environments](https://github.com/feiss/aframe-environment-component)
* [FUTEL](http://futel.net/)